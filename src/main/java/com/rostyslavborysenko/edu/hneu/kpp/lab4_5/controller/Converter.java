package com.rostyslavborysenko.edu.hneu.kpp.lab4_5.controller;

import com.rostyslavborysenko.edu.hneu.kpp.lab4_5.model.Student;
import com.rostyslavborysenko.edu.hneu.kpp.lab4_5.model.StudentModel;

public class Converter {
    public static void studentEntityToStudentModel(StudentModel model, Student student) {
        model.setId(student.getId());
        model.setFirstName(student.getFirstName());
        model.setLastName(student.getLastName());
        model.setMiddleName(student.getMiddleName());
        model.setSex(student.getSex());
        model.setPhoneNumber(student.getPhoneNumber());
        model.setEmail(student.getEmail());
        model.setSpeciality(student.getSpeciality());
        model.setGroupNumber(student.getGroupNumber());
        model.setCourse(student.getCourse());
        model.setDetails(student.getDetails());
    }

    public static void studentModelToStudentEntity(Student student, StudentModel model) {
        //student.setId(model.getId());
        student.setFirstName(model.getFirstName());
        student.setLastName(model.getLastName());
        student.setMiddleName(model.getMiddleName());
        student.setSex(model.getSex());
        student.setPhoneNumber(model.getPhoneNumber());
        student.setEmail(model.getEmail());
        student.setSpeciality(model.getSpeciality());
        student.setGroupNumber(model.getGroupNumber());
        student.setCourse(model.getCourse());
        student.setDetails(model.getDetails());
    }
}
