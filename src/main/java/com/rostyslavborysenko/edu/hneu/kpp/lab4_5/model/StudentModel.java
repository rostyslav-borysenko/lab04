package com.rostyslavborysenko.edu.hneu.kpp.lab4_5.model;


public class StudentModel {
    private Integer id;
    private String firstName;
    private String lastName;
    private String middleName;
    private String sex;
    private String phoneNumber;
    private String email;
    private String speciality;
    private String groupNumber;
    private Integer course;
    private String details;

    public StudentModel () {
    }

    public StudentModel(String firstName, String lastName, String middleName,
                   String sex, String phoneNumber, String email, String speciality,
                   String groupNumber, int course, String details) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.middleName = middleName;
        this.sex = sex;
        this.phoneNumber = phoneNumber;
        this.email = email;
        this.speciality = speciality;
        this.groupNumber = groupNumber;
        this.course = course;
        this.details = details;
    }

    public Integer getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstfirstNamename) {
        this.firstName = firstfirstNamename;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSpeciality() {
        return speciality;
    }

    public void setSpeciality(String speciality) {
        this.speciality = speciality;
    }

    public String getGroupNumber() {
        return groupNumber;
    }

    public void setGroupNumber(String groupNumber) {
        this.groupNumber = groupNumber;
    }

    public Integer getCourse() {
        return course;
    }

    public void setCourse(Integer course) {
        this.course = course;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    @Override
    public String toString() {
        return String.format("Student [ id=%s, name=%s %s %s, sex=%s, phoneNumber=%s, email=%s, " +
                        "speciality=%s, groupNumber=%s, course=%s, details=%s]", id, firstName, lastName,
                middleName, sex, phoneNumber, email, speciality, groupNumber, course, details);
    }
}
