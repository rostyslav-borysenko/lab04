package com.rostyslavborysenko.edu.hneu.kpp.lab4_5.dao;

import com.rostyslavborysenko.edu.hneu.kpp.lab4_5.model.Student;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import java.util.List;

public class StudentDao implements StudentDaoInterface<Student, Integer> {
    private Session currentSession;
    private Transaction currentTransaction;

    public Session openCurrentSession() {
        currentSession = getSessionFactory().openSession();
        return currentSession;
    }

    public Session openCurrentSessionWithTransaction() {
        currentSession = getSessionFactory().openSession();
        currentTransaction = currentSession.beginTransaction();
        return currentSession;
    }

    public void closeCurrentSession() {
        currentSession.close();
    }

    public void closeCurrentSessionWithTransaction() {
        currentTransaction.commit();
        currentSession.close();
    }

    private static SessionFactory getSessionFactory() {
        Configuration configuration = new Configuration().configure("META-INF/hibernate.cfg.xml");
        //StandardServiceRegistry standardRegistry = new StandardServiceRegistryBuilder().configure("hibernate.cfg.xml").build();
        //Metadata metadata = new MetadataSources(standardRegistry).getMetadataBuilder().build();
        //return metadata.getSessionFactoryBuilder().build();
        return configuration.buildSessionFactory();
    }

    public Session getCurrentSession() {
        return currentSession;
    }

    public void setCurrentSession(Session currentSession) {
        this.currentSession = currentSession;
    }

    public Transaction getCurrentTransaction() {
        return currentTransaction;
    }

    public void setCurrentTransaction(Transaction currentTransaction) {
        this.currentTransaction = currentTransaction;
    }

    @Override
    public void persist(Student entity) {
        getCurrentSession().save(entity);
    }

    @Override
    public void update(Student entity) {
        getCurrentSession().update(entity);
    }

    @Override
    public Student findById(Integer id) {
        return getCurrentSession().get(Student.class, id);
    }

    @Override
    public void delete(Student entity) {
        getCurrentSession().delete(entity);
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Student> findAll() {
        return (List<Student>) getCurrentSession().createQuery("from Student", Student.class).getResultList();
    }

    @Override
    public void deleteAll() {
        List<Student> entityList = findAll();
        for(Student entity : entityList) {
            delete(entity);
        }
    }
}


