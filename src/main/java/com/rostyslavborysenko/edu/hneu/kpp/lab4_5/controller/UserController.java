package com.rostyslavborysenko.edu.hneu.kpp.lab4_5.controller;

import com.rostyslavborysenko.edu.hneu.kpp.lab4_5.model.Student;
import com.rostyslavborysenko.edu.hneu.kpp.lab4_5.model.StudentModel;
import com.rostyslavborysenko.edu.hneu.kpp.lab4_5.service.StudentService;
import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@WebServlet(urlPatterns = "/UserController")
public class UserController extends HttpServlet {

    private static final long serialVersionUID = 1L;
    private static final String INSERT_OR_EDIT = "pages/insert.jsp";
    private static final String LIST_STUDENT = "pages/showAll.jsp";
    private final StudentService studentService;

    public UserController() {
        super();
        studentService = new StudentService();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String forward="";
        String action = request.getParameter("action");

        if (action.equalsIgnoreCase("delete")){
            Integer studentId = Integer.parseInt(request.getParameter("id"));
            studentService.delete(studentId);
            forward = LIST_STUDENT;
            request.setAttribute("students", studentService.findAll());
        } else if (action.equalsIgnoreCase("edit")){
            forward = INSERT_OR_EDIT;
            Integer studentId = Integer.parseInt(request.getParameter("id"));
            Student student = studentService.findById(studentId);
            request.setAttribute("students", student);
            request.setAttribute("action", "edit");
        } else if (action.equalsIgnoreCase("showAll")){
            forward = LIST_STUDENT;
            List<Student> studentList = studentService.findAll();
            request.setAttribute("students", studentList);
        } else {
            forward = INSERT_OR_EDIT;
            request.setAttribute("action", "insert");
        }

        RequestDispatcher view = request.getRequestDispatcher(forward);
        view.forward(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        StudentModel studentModel = new StudentModel();
        studentModel.setFirstName(request.getParameter("firstName"));
        studentModel.setLastName(request.getParameter("lastName"));
        studentModel.setMiddleName(request.getParameter("middleName"));
        studentModel.setSex(request.getParameter("sex"));
        studentModel.setPhoneNumber(request.getParameter("phoneNumber"));
        studentModel.setEmail(request.getParameter("email"));
        studentModel.setSpeciality(request.getParameter("speciality"));
        studentModel.setGroupNumber(request.getParameter("groupNumber"));
        studentModel.setCourse(Integer.parseInt(request.getParameter("course")));
        studentModel.setDetails(request.getParameter("details"));

        List<String> errors = new ArrayList<>();

        if(studentModel.getFirstName().length() > 20) {
            errors.add("First name can not be longer than 20 letters");
        }

        if(studentModel.getLastName().length() > 30) {
            errors.add("Last name can not be longer than 30 letters");
        }

        if(studentModel.getMiddleName().length() > 30) {
            errors.add("Middle name can not be longer than 30 letters");
        }

        if(studentModel.getPhoneNumber().length() > 13) {
            errors.add("Phone number can not be longer than 13 characters");
        }

        if(studentModel.getEmail().length() > 50) {
            errors.add("Email can not be longer than 50 characters");
        }

        if(studentModel.getSpeciality().length() > 50) {
            errors.add("Speciality can not be longer than 50 characters");
        }

        if(studentModel.getGroupNumber().length() > 50) {
            errors.add("Group number can not be longer than 50 characters");
        }

        Integer course = studentModel.getCourse();
        if(course < 1 || course > 6) {
            errors.add("Course must be between 1 and 6.");
        }

        if(studentModel.getDetails().length() > 255) {
            errors.add("Details can not be longer than 255 characters");
        }

        if(!errors.isEmpty()) {
            request.setAttribute("errors", errors);
            RequestDispatcher view = request.getRequestDispatcher(INSERT_OR_EDIT);
            view.forward(request, response);
            return;
        }

        String idString = request.getParameter("id");

        if(idString == null || idString.isEmpty()) {
            Student studentEntity = new Student();
            Converter.studentModelToStudentEntity(studentEntity, studentModel);
            studentService.persist(studentEntity);
        } else {
            Integer id = Integer.parseInt(idString);
            Student studentEntity = studentService.findById(id);
            if(studentEntity == null) {
                request.setAttribute("errorMessage", "Declaration not found.");
                RequestDispatcher view = request.getRequestDispatcher(LIST_STUDENT);
                view.forward(request, response);
                return;
            }
            Converter.studentModelToStudentEntity(studentEntity, studentModel);
            studentService.update(studentEntity);
        }

        RequestDispatcher view = request.getRequestDispatcher(LIST_STUDENT);
        request.setAttribute("students", studentService.findAll());
        view.forward(request, response);
    }
}
