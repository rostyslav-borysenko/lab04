package com.rostyslavborysenko.edu.hneu.kpp.lab4_5.model;

import jakarta.persistence.*;

@Entity
@Table (name = "student")
public class Student {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private Integer id;

    @Column(name = "first_name", nullable = false)
    private String firstName;

    @Column(name = "last_name", nullable = false)
    private String lastName;

    @Column(name = "middle_name", nullable = false)
    private String middleName;

    @Column(name = "sex")
    private String sex;

    @Column(name = "phone_number")
    private String phoneNumber;

    @Column(name = "email")
    private String email;

    @Column(name = "speciality", nullable = false)
    private String speciality;

    @Column(name = "group_number", nullable = false)
    private String group_number;

    @Column(name = "course", nullable = false)
    private Integer course;

    @Column(name = "details")
    private String details;

    public Student () {
    }

    public Student(String first_name, String last_name, String middle_name,
                   String sex, String phone_number, String email, String speciality,
                   String group_number, int course, String details) {
        this.firstName = first_name;
        this.lastName = last_name;
        this.middleName = middle_name;
        this.sex = sex;
        this.phoneNumber = phone_number;
        this.email = email;
        this.speciality = speciality;
        this.group_number = group_number;
        this.course = course;
        this.details = details;
    }

    public Integer getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String first_name) {
        this.firstName = first_name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String last_name) {
        this.lastName = last_name;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middle_name) {
        this.middleName = middle_name;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phone_number) {
        this.phoneNumber = phone_number;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSpeciality() {
        return speciality;
    }

    public void setSpeciality(String speciality) {
        this.speciality = speciality;
    }

    public String getGroupNumber() {
        return group_number;
    }

    public void setGroupNumber(String group_number) {
        this.group_number = group_number;
    }

    public Integer getCourse() {
        return course;
    }

    public void setCourse(Integer course) {
        this.course = course;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    @Override
    public String toString() {
        return String.format("Student [ id=%s, name=%s %s %s, sex=%s, phone_number=%s, email=%s, " +
                "speciality=%s, group_number=%s, course=%s, details=%s]", id, firstName, lastName,
                middleName, sex, phoneNumber, email, speciality, group_number, course, details);
    }
}