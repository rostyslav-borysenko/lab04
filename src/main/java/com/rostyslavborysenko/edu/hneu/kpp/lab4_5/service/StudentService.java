package com.rostyslavborysenko.edu.hneu.kpp.lab4_5.service;

import com.rostyslavborysenko.edu.hneu.kpp.lab4_5.dao.StudentDao;
import com.rostyslavborysenko.edu.hneu.kpp.lab4_5.model.Student;

import java.util.List;

public class StudentService {

    private static StudentDao studentDao;
    public StudentService () {
        studentDao = new StudentDao();
    }

    public void persist (Student entity) {
        studentDao.openCurrentSessionWithTransaction();
        studentDao.persist(entity);
        studentDao.closeCurrentSessionWithTransaction();
    }

    public void update (Student entity) {
        studentDao.openCurrentSessionWithTransaction();
        studentDao.update(entity);
        studentDao.closeCurrentSessionWithTransaction();
    }

    public Student findById(Integer id) {
        studentDao.openCurrentSession();
        Student student = studentDao.findById(id);
        studentDao.closeCurrentSession();
        return student;
    }

    public void delete(Integer id) {
        studentDao.openCurrentSessionWithTransaction();
        Student student = studentDao.findById(id);
        studentDao.delete(student);
        studentDao.closeCurrentSessionWithTransaction();
    }

    public List<Student> findAll() {
        studentDao.openCurrentSessionWithTransaction();
        List<Student> students = studentDao.findAll();
        studentDao.closeCurrentSessionWithTransaction();
        return students;
    }

    public void deleteAll() {
        studentDao.openCurrentSessionWithTransaction();
        studentDao.deleteAll();
        studentDao.closeCurrentSessionWithTransaction();
    }

    public StudentDao studentDao() {
        return studentDao;
    }
}

