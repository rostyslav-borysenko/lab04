<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Welcome to Student Management System</title>
    <link rel="stylesheet" type="text/css" href="css/styles.css">
</head>
<body>
<div class="container">
    <div class="logo-container">
        <img src="https://www.hneu.edu.ua/wp-content/uploads/2021/04/novoe-logo-IT.png" alt="logo-IT" width="75" height="75">
        <h1 class="logo-text">Information Technology</h1>
    </div>

    <h2>Welcome to Student Management System</h2>
    <div class="wrapper-btn">
        <a class="button" href="UserController?action=showAll" style="margin-right: 10px">Show All Students</a>
        <a class="button" href="UserController?action=insert">Insert New Student</a>
    </div>
</div>
</body>
</html>
