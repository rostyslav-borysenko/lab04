<%@ page import="com.rostyslavborysenko.edu.hneu.kpp.lab4_5.model.Student" %>
<%@ page import="java.util.List" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Show All Students</title>
    <link rel="stylesheet" type="text/css" href="../css/styles.css">
</head>
<body>
<div class="container">
    <div class="container-table">
        <a href="/" class="button">Home</a>
        <h2>All Students</h2>
        <a href="UserController?action=insert" class="button">Insert new student</a>
    </div>
    <table>
        <tr>
            <th>Id</th>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Middle Name</th>
            <th>Sex</th>
            <th>Phone Number</th>
            <th>Email</th>
            <th>Speciality</th>
            <th>Group Number</th>
            <th>Course</th>
            <th>Details</th>
            <th></th>
        </tr>
        <%
            List<Student> students = (List<Student>) request.getAttribute("students");
            for (Student student : students) {
        %>
        <tr>
            <td><%= student.getId() %></td>
            <td><%= student.getFirstName() %></td>
            <td><%= student.getLastName() %></td>
            <td><%= student.getMiddleName() %></td>
            <td><%= student.getSex() %></td>
            <td><%= student.getPhoneNumber() %></td>
            <td><%= student.getEmail() %></td>
            <td><%= student.getSpeciality() %></td>
            <td><%= student.getGroupNumber() %></td>
            <td><%= student.getCourse() %></td>
            <td><%= student.getDetails() %></td>
            <td>
                <a class="edit-btn" href="UserController?action=edit&id=<%= student.getId() %>">Edit</a>
                <a class="delete-btn" href="UserController?action=delete&id=<%= student.getId() %>">Delete</a>
            </td>
        </tr>
        <% } %>
    </table>
</div>
</body>
</html>
