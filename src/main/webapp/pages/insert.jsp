<%@ page import="java.util.List" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
    List<String> errors = (List<String>) request.getAttribute("errors");
%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Manage Student</title>
    <link rel="stylesheet" type="text/css" href="../css/styles.css">
</head>
<body>
<div class="container-manage">
    <div class="container-table" style="margin-bottom: 30px">
        <a href="/" class="button">Home</a>
        <h2>Manage Student</h2>
        <a href="UserController?action=showAll" class="button">All students</a>
    </div>
    <% if (errors != null && !errors.isEmpty()) {
        for(String errorMessage : errors) {
    %>
    <div class="error-message">*<%= errorMessage %></div>
    <% }} %>
    <form id="studentForm" action="UserController?action=showAll" method="post">
        <input type="hidden" name="id" value="${empty students ? '' : students.id}">
        <div>
            <label for="firstName">First Name*</label>
            <input type="text" id="firstName" name="firstName" value="${empty students ? '' : students.firstName}" required><br>
        </div>
        <div>
            <label for="lastName">Last Name*</label>
            <input type="text" id="lastName" name="lastName" value="${empty students ? '' : students.lastName}" required><br>
        </div>
        <div>
            <label for="middleName">Middle Name</label>
            <input type="text" id="middleName" name="middleName" value="${empty students ? '' : students.middleName}"><br>
        </div>
        <div>
            <label for="sex">Sex</label>
            <select id="sex" name="sex">
                <option value="Male" ${empty students || students.sex ne 'Female' ? 'selected' : ''}>Male</option>
                <option value="Female" ${!empty students && students.sex eq 'Female' ? 'selected' : ''}>Female</option>
            </select><br>
        </div>
        <div>
            <label for="phoneNumber">Phone Number</label>
            <input type="text" id="phoneNumber" name="phoneNumber" value="${empty students ? '' : students.phoneNumber}"><br>
        </div>
        <div>
            <label for="email">Email</label>
            <input type="email" id="email" name="email" value="${empty students ? '' : students.email}"><br>
        </div>
        <div>
            <label for="speciality">Speciality*</label>
            <input type="text" id="speciality" name="speciality" value="${empty students ? '' : students.speciality}" required><br>
        </div>
        <div>
            <label for="groupNumber">Group Number*</label>
            <input type="text" id="groupNumber" name="groupNumber" value="${empty students ? '' : students.groupNumber}" required><br>
        </div>
        <div>
            <label for="course">Course*</label>
            <input type="number" id="course" name="course" value="${empty students ? '' : students.course}" required><br>
        </div>
        <div>
            <label for="details">Details</label>
            <textarea class="textarea-details" id="details" name="details">${empty students ? '' : students.details}</textarea><br>
        </div>
        <div>
            <button type="submit" class="button" style="margin-right: 5px">Submit</button>
            <button type="button" class="button" onclick="clearForm()">Clear</button>
        </div>
    </form>

    <script>
        function clearForm() {
            var form = document.getElementById("studentForm");

            var elements = form.elements;
            for (var i = 0; i < elements.length; i++) {
                var elementType = elements[i].type.toLowerCase();
                switch (elementType) {
                    case "text":
                    case "email":
                    case "textarea":
                    case "number":
                        elements[i].value = "";
                        break;
                    default:
                        break;
                }
            }
        }
    </script>
</div>
</body>
</html>
